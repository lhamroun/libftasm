%ifdef __LINUX__
	%define SYSCALL_INDEX_WRITE 1
	%define FIND_ERRNO __errno_location
	%define ERRNO_ARG wrt ..plt
	%define WRITE ft_write
%else
	%define SYSCALL_INDEX_WRITE 0x2000004
	%define FIND_ERRNO ___error
	%define ERRNO_ARG
	%define WRITE _ft_write
%endif

	section .text
	global WRITE
	extern FIND_ERRNO

WRITE:
	mov rax, SYSCALL_INDEX_WRITE
	syscall

%ifdef __LINUX__
	cmp rax, 0
	jl error
%else
	jc error
%endif
	ret

error:
%ifdef __LINUX__
	neg rax
%endif
	push rax
	call FIND_ERRNO ERRNO_ARG
	pop qword[rax]
	mov rax, -1
	ret
