/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/19 17:08:33 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/05/26 16:56:04 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftasm.h"

//#define ONLY_MY_FUNC

void	test_ft_strlen(char *str)
{
#ifndef ONLY_MY_FUNC
	printf("\nreal  strlen = %lu\n", strlen(str));
	write(1, "", 0);
#endif
	printf("my ft_strlen = %lu\n\n", ft_strlen(str));
}

void	test_ft_strcmp(char *s1, char *s2)
{
#ifndef ONLY_MY_FUNC
	printf("\nreal strcmp %d\n", strcmp(s1, s2));
	write(1, "", 0);
#endif
	printf("my   strcmp %d\n", ft_strcmp(s1, s2));
}

void	test_ft_strcpy(char *s1, char *s2)
{
	char *s3 = strdup(s1);
	char *s4 = strdup(s2);
#ifndef ONLY_MY_FUNC
	printf("\nreal  strcpy = %s\n", strcpy(s3, s4));
	free(s3);
	free(s4);
	s3 = strdup(s1);
	s4 = strdup(s2);
	write(1, "", 0);
#endif
	printf("my    strcpy = %s\n", ft_strcpy(s3, s4));
	free(s3);
	free(s4);
}

void	test_ft_strdup(char *str)
{
	char * test = NULL;
#ifndef ONLY_MY_FUNC
	test = strdup(str);
	printf("\nreal strdup %s\n", test);
	if (test)
		free(test);
	write(1, "", 0);
#endif
	test = ft_strdup(str);
	printf("my   strdup %s\n", test);
		free(test);
}

void	test_ft_write(char *file, char *str, int size)
{
	int result;
	int err;
	int fd = -1;

#ifndef ONLY_MY_FUNC
	fd = open(file, O_RDWR);
	if (fd == -1)
		fd = 1;
	result = write(fd, str, size);
	printf("\nreal write : %d\n", result);
#ifdef __APPLE__
	err = *__error();
#else
	err = *__errno_location();
#endif
	if (err)
		printf("errno return : %s\n", strerror(err));
	if (fd != 1)
		close(fd);
	write(1, "", 0);
#endif
	fd = open(file, O_RDWR);
	if (fd == -1)
		fd = 1;
	result = ft_write(fd, str, size);
	printf("\nmy   write : %d\n", result);
#ifdef __APPLE__
	err = *__error();
#else
	err = *__errno_location();
#endif
	if (err)
		printf("errno return : %s\n", strerror(err));
	if (fd != 1)
		close(fd);
}

void	test_ft_read(int fd, char *str, int size)
{
	char buf[size];
	int result;
	int err;

#ifndef ONLY_MY_FUNC
	if (fd != 1)
		fd = open(str, O_RDONLY);
	result = read(fd, buf, size);
	printf("\nreal read : %d - %s\n", result, buf);
#ifdef __APPLE__
	err = *__error();
#else
	err = *__errno_location();
#endif
	if (err)
		printf("errno return : %s\n", strerror(err));
	if (fd != 1)
		close(fd);
	write(1, "", 0);
#endif
	if (fd != 1)
		fd = open(str, O_RDONLY);
	result = ft_read(fd, buf, size);
	printf("\nmy   read : %d - %s\n", result, buf);
#ifdef __APPLE__
	err = *__error();
#else
	err = *__errno_location();
#endif
	if (err)
		printf("errno return : %s\n", strerror(err));
	if (fd != 1)
		close(fd);
}

int main(int ac, char **av)
{
	if (ac == 1)
		printf("usage: ./tester [function name] [arg1] [arg2]...\n");
	else
	{
		if (!strcmp("strlen", av[1]) && ac == 3)
			test_ft_strlen(av[2]);
		else if (!strcmp("strcmp", av[1]) && ac == 4)
			test_ft_strcmp(av[2], av[3]);
		else if (!strcmp("strcpy", av[1]) && ac == 4)
			test_ft_strcpy(av[2], av[3]);
		else if (!strcmp("strdup", av[1]) && ac == 3)
			test_ft_strdup(av[2]);
		else if (!strcmp("write", av[1]) && ac == 5)
			test_ft_write(av[2], av[3], atoi(av[4])); // remettre le av[2] variable en fonction du fd d'entree
		else if (!strcmp("read", av[1]) && ac == 5)
			test_ft_read(atoi(av[2]), av[3], atoi(av[4]));
		else
			printf("usage: ./tester [strlen | strcmp | strcpy | strdup | wite | read] [arg1] [arg2]...\n");
	}

	return 0;
}
