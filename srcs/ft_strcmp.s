%ifdef __LINUX__
	%define STRCMP ft_strcmp
%else
	%define STRCMP _ft_strcmp
%endif

	section .text
	global STRCMP

STRCMP:
	push rbp
	mov rbp, rsp
	xor rcx, rcx

_loop:
; We check if rcx's index is '\0'
	mov al, BYTE[rdi + rcx]
	mov bl, BYTE[rsi + rcx]
	cmp al, 0x00
	jz end
	cmp bl, 0x00
	jz end

	cmp al, bl
	jne end

	inc rcx
	jmp _loop

end:
	movzx rax, al
	movzx rbx, bl
	sub rax, rbx

	mov rsp, rbp
	pop rbp
	ret
