%ifdef __LINUX__
	%define STRLEN ft_strlen
%else
	%define STRLEN _ft_strlen
%endif

section .text
	global STRLEN

STRLEN:
; prologue
	push rbp
	mov rbp, rsp
	mov rcx, -1

len:
	inc rcx
	cmp byte [rdi + rcx], 0
	jne len

; set result on rax
	mov rax, rcx

; epilogue
	mov rsp, rbp
	pop rbp
	ret
