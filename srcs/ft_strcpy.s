%ifdef __LINUX__
	%define STRCPY ft_strcpy
%else
	%define STRCPY _ft_strcpy
%endif

	section .text
	global STRCPY

; char *ft_strcpy(char *s1, char *s2)
STRCPY:
;prologue
	push rbp
	mov rbp, rsp
	xor rcx, rcx

; check input
	cmp rdi, 0
	jz error
	cmp rsi, 0
	jz error

	jmp while

; copy each char on dest
while:
	cmp BYTE[rsi + rcx], 0
	je end
	mov al, BYTE[rsi + rcx]
	mov BYTE[rdi + rcx], al
	inc rcx
	jmp while

; if an error occur
error:
	xor rax, rax
; epilogue
	mov rsp, rbp
	pop rbp
	ret

end:
	mov BYTE[rdi + rcx], 0
	mov rax, rdi
; epilogue
	mov rsp, rbp
	pop rbp
	ret
