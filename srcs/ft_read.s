%ifdef __LINUX__
	%define SYSCALL_INDEX_READ 0
	%define FIND_ERRNO __errno_location
	%define ERRNO_ARG wrt ..plt
	%define READ ft_read
%else
	%define SYSCALL_INDEX_READ 0x2000003
	%define FIND_ERRNO ___error
	%define ERRNO_ARG
	%define READ _ft_read
%endif

	section .text
	global READ
	extern FIND_ERRNO

READ:
	mov rax, SYSCALL_INDEX_READ
	syscall

%ifdef __LINUX__
	cmp rax, 0
	jl error_read
%else
	jc	error_read
%endif
	ret

error_read:
%ifdef __LINUX__
	neg rax
%endif
	push rax
	call FIND_ERRNO ERRNO_ARG
	pop qword[rax]
	mov rax, -1
	ret
