%ifdef __LINUX__
	%define STRDUP ft_strdup
	%define MALLOC malloc
	%define MALLOC_ARG wrt ..plt
	%define STRCPY ft_strcpy
	%define STRLEN ft_strlen
%else
	%define STRDUP _ft_strdup
	%define MALLOC _malloc
	%define MALLOC_ARG
	%define STRCPY _ft_strcpy
	%define STRLEN _ft_strlen
%endif

	section .text
	global STRDUP
	extern MALLOC
	extern STRLEN
	extern STRCPY

; char *ft_strdup(const char *str)
STRDUP:
;prologue
;	push rbp
;	mov rbp, rsp

; count the number of bytes on rdi
	call STRLEN
; save rdi value
	push rdi
; set malloc parameter
	inc rax
	mov rdi, rax

; call malloc with the rax + 1 bytes to allocate
	call MALLOC MALLOC_ARG

; if malloc fail, go to error
	cmp rax, 0
	jz error

; call strcpy with rax address on dest
	mov rdi, rax
; call strcpy
	pop rsi
	call STRCPY

; epilogue
;	mov rsp, rbp
;	pop rbp
	ret

; error label ret NULL
error:
	pop rdi

; epilogue
;	mov rsp, rbp
;	pop rbp
	ret
