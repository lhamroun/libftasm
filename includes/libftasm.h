#ifndef LIBFTASM_H
# define LIBFTASM_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include <errno.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>

size_t	ft_strlen(const char *str);
char	*ft_strcpy(char *dest, const char *src);
ssize_t	ft_write(int fd, const void *ptr, size_t len);
int		ft_strcmp(const char *s1, const char *s2);
ssize_t	ft_read(int fd, void *buf, size_t size);
char	*ft_strdup(const char *str);

# endif
