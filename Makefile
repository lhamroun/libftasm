# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/05/19 17:08:07 by lyhamrou          #+#    #+#              #
#    Updated: 2021/05/26 14:26:53 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libasm.a
COMPILER = nasm

OS_NAME = $(shell uname)

FLAGS = -f
ifeq ($(OS_NAME),Darwin)
	FLAGS += macho64
else
	FLAGS += elf64 -D __LINUX__=1
endif

INCLUDES = -I ./includes/
ASM_LIB = -L. -lasm

SRCS_DIR = srcs/
SRCS_NAME = ft_write.s ft_strlen.s ft_strcmp.s ft_read.s ft_strcpy.s ft_strdup.s
SRCS = $(addprefix $(SRCS_DIR), $(SRCS_NAME))

OBJS_DIR = .objs/
OBJS_NAME = $(SRCS_NAME:.s=.o)
OBJS = $(addprefix $(OBJS_DIR), $(OBJS_NAME))

TEST_NAME = tester

all : $(NAME)

$(NAME) : $(OBJS_DIR) $(OBJS)
	ar rcs $(NAME) $(OBJS)
	ranlib $(NAME)

$(OBJS_DIR) :
	mkdir -p $(OBJS_DIR)

$(OBJS_DIR)%.o : $(SRCS_DIR)%.s
	@echo compiling $< to $@
	$(COMPILER) $(FLAGS) -o $@ $<

fclean : clean
	$(RM) -rf $(OBJS_DIR)

clean :
	$(RM) $(NAME)
	$(RM) $(TEST_NAME)

re : fclean all

$(TEST_NAME): re
	gcc -o .objs/main.o -c srcs/main.c $(INCLUDES)
	gcc -o $(TEST_NAME) .objs/main.o $(ASM_LIB)

.PHONY : all clean fclean re
